<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index() {
        $users = DB::table('users')->get();

        $id = Auth::user()->id;
        $admins = DB::table('users')->where('id', '=', $id)->get();
        
        return view('auth.admin', ['users' => $users, 'admins' => $admins]);
    }
}
