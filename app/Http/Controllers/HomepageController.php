<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomepageController extends Controller
{
    public function index() {
        if(Auth::check()){
            $id = Auth::user()->id;
            $admins = DB::table('users')->where('id', '=', $id)->get();
        }else{
            $admins = 0;
        }

        
        

        return view('homepage', ['admins' => $admins]);
    }
}
