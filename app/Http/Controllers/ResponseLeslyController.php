<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ResponseLeslyController extends Controller
{
    public function index() {

        $id = Auth::user()->id;
        $admins = DB::table('users')->where('id', '=', $id)->get();

        return view('responseLesly', ['admins' => $admins]);
    }
}
