<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HttpRequest;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;



class JsonController extends Controller
{
    
    public function JsonMaker(request $data){
        $newurl = addcslashes($data->urlDb, '/');
        $newurl2 = addcslashes($newurl, '/');
        $newurl3 = addcslashes($newurl2, '/');
        $jsonencode = '{"nargout":1,"rhs":"{\"urlDb\": \"'.$newurl3.'\",\"dbName\": \"'.$data->dbName.'\",\"measurement\":\"'.$data->measurement.'\",\"entityOperation\": \"'.$data->entityOperation.'\",\"information\": \"'.$data->information.'\",\"trainingType\": \"'.$data->trainingType.'\",\"dateStart\": \"'.$data->dateStart.'\",\"hourStart\": \"'.$data->hourStart.'\",\"tzStart\": \"'.$data->tzStart.'\",\"dateEnd\": \"'.$data->dateEnd.'\",\"hourEnd\": \"'.$data->hourEnd.'\",\"tzEnd\": \"'.$data->tzEnd.'\",\"label\": \"'.$data->label.'\",\"serverIP\": \"'.$data->serverIP.'\",\"serverUsername\": \"'.$data->serverUsername.'\",\"serverPassword\": \"'.$data->serverPassword.'\"}"}';


        $id = Auth::user()->id;
        $admins = DB::table('users')->where('id', '=', $id)->get();


        return view('reponse', compact(array('jsonencode', 'data', 'admins')));
    }
}
