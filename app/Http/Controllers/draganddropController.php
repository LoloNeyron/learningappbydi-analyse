<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DraganddropController extends Controller
{
    public function index() {
        if(Auth::check()){
            $id = Auth::user()->id;
            $admins = DB::table('users')->where('id', '=', $id)->get();
        }else{
            $admins = 0;
        }


        return view('dragAndDrop', ['admins' => $admins]);
    }

    public function res() {
        if(Auth::check()){
            $id = Auth::user()->id;
            $admins = DB::table('users')->where('id', '=', $id)->get();
        }else{
            $admins = 0;
        }


        return view('ResponseOfAjax', ['admins' => $admins]);
    }
}
