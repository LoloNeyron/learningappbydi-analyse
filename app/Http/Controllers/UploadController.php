<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Redirect;

class UploadController extends Controller
{
    public function store(Request $request) {

        $extension = $request->file('files')->getClientOriginalExtension();

        $fileNameWithExtension = $request->file('files')->getClientOriginalName();

        if($extension === 'jar'){
            Storage::disk('sftp')->put($fileNameWithExtension, fopen($request->file('files'), 'r+'));
        }else if($extension === "mat"){
            Storage::disk('sftp2')->put($fileNameWithExtension, fopen($request->file('files'), 'r+'));
        }else{
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }

        return Redirect::back()->withErrors(['msg', 'The Message']);
    }
}
