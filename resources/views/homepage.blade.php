@extends('layouts.app')

@section('headerContent')
    @include('cssPage.homepagecss')
@endsection

@section('content')



<div class="container-fluid">
    <div class="row">
        <div class="col-2 hide-on-med-and-down"></div>
        <div class="col-lg-8 col-md-12">
            <div class="container">
                    <div class="row center">
                        <div class="col-12 center">
                            @if(Session::has('message'))
                                <p class="alert alert-success"><strong>{{ Session::get('message') }}</strong></p>
                            @endif
                            <img src=".\assets\images\Lesly_logo.png"  width="160px" height="160px" class="img-fluid" alt="Logo Lesly">
                        </div>
                    </div>
                </div>
            <div class="card greyCyclo">
                <div class="card-content white-text">
                    <span class="card-title center"><hr><h1>Learning App</h1><hr></span>
                    @if(Auth::check())
                        <h4>Bonjour {{Auth::user()->name}}</h4>
                        <br>


                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" style='width: 50%'>    
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Apprentissage</a>
                            </li>
                            <li class="nav-item" style='width: 50%'>
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Mise a jour Lesly</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form class="col-12" action={{Route('JsonMaker')}} method="POST">
                            @csrf
                            <input type="hidden" name="urlDb" value="{{Auth::user()->urlInfluxDB}}">
                            <input type="hidden" name="serverIP" value="{{Auth::user()->IpServer}}">
                            <input type="hidden" name="serverUsername" value="{{Auth::user()->LPSUser}}">
                            <input type="hidden" name="serverPassword" value="{{Auth::user()->LPSPassword}}">


                            <div class="accordion" id="accordionbdd">
                                <div class="card grey8">
                                    <button class="btn waves-red btn-link grey grey7 darken-4" type="button" data-toggle="collapse" data-target="#bdd" aria-expanded="false" aria-controls="bdd">
                                        <div class="card-header" id="headingOne">
                                            <h6 class="red-text text-darken-2 mb-0">
                                                Base de Donnees
                                            </h6>
                                        </div>
                                    </button>
                                    <div id="bdd" class="collapse myTextWhite" aria-labelledby="headingOne" data-parent="#accordionbdd">
                                        <div class="card-body myTextWhite">
                                            <div class="row">
                                                <div class="col-4 valign-wrapper ">
                                                    <label for="dbName ">Nom de la base de données :</label>
                                                </div>
                                                <div class="input-flied col-8 myTextWhite">
                                                    <select required name="dbName" id="dbName" class="" style="color: white">
                                                        <option value="monitoring">monitoring</option>
                                                        <option value="learning">learning</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="measurement">Le measurement</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="ex : onlineLearning" name="measurement" id="measurement" type="text" class="validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="entityOperation">L’entity Operation</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="ex : v50" name="entityOperation" id="entityOperation" type="text" class="validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="information">L’information</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="ex : sain" name="information" id="information" type="text" class="validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="trainingType">Type d’entrainement</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <select required name="trainingType" class="Special" id="trainingType">
                                                        <option value="supervised">supervised</option>
                                                        <option value="unsupervised">unsupervised</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="label">Le vrai label des données (dans le cas du ‘unsupervised’ seulement.)</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input placeholder="ex : defaut_balourd" name="label" id="label" type="text" class="validate input-field">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card grey8">
                                    <button class="btn btn-link collapsed waves-red grey grey7 darken-4" type="button" data-toggle="collapse" data-target="#CollExtract" aria-expanded="false" aria-controls="CollExtract">
                                        <div class="card-header" id="headingExtract">
                                            <h6 class="red-text text-darken-2 mb-0">
                                                Lancement
                                            </h6>
                                        </div>
                                    </button>
                                    <div id="CollExtract" class="collapse" aria-labelledby="headingExtract" data-parent="#accordionbdd">
                                        <div class="card-body">
                                            <h5>Début</h5>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="dateStart">Date de début pour l’extraction de la table :</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="Choisir une date ..." name="dateStart" id="dateStart" type="text" class="datepicker validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="hourStart">Heure de début :</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="Choisir une heure ..." name="hourStart" id="hourStart" type="time" class=" validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="tzStart">Heure d’été ou heure d’hiver de debut</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <select required name="tzStart" class="Special" id="tzStart">
                                                        <option value="winter">winter</option>
                                                        <option value="summer">summer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h5>Fin</h5>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="dateEnd">Date de fin pour l’extraction de la table :</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="Choisir une date ..." name="dateEnd" id="dateEnd" type="text" class="datepicker validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="hourEnd">Heure de fin :</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <input required placeholder="Choisir une heure ..." name="hourEnd" id="hourEnd" type="time" class="validate input-field">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 valign-wrapper">
                                                    <label for="tzEnd">Heure d’été ou heure d’hiver de fin :</label>
                                                </div>
                                                <div class="input-flied col-8">
                                                    <select required name="tzEnd" class="Special" id="tzEnd">
                                                        <option value="winter">winter</option>
                                                        <option value="summer">summer</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    
                                        @foreach($admins as $admin)
                                         @if($admin->isAdmin)
                                         <div class="card grey8">
                                            <button class="btn btn-link collapsed waves-red grey grey7 darken-4" type="button" data-toggle="collapse" data-target="#collapseLPS" aria-expanded="false" aria-controls="collapseLPS">
                                                <div class="card-header" id="headingLPS">
                                                    <h6 class="red-text text-darken-2 mb-0">
                                                        LPS Info
                                                    </h6>
                                                </div>
                                            </button>
                                            <div id="collapseLPS" class="collapse" aria-labelledby="headingLPS" data-parent="#accordionbdd">
                                                <div class="card-body">
                                                    
                                                </div>
                                    </div>
                                            </div>
                                        @endif
                                        @endforeach
                                </div>
                            <button type="submit" class="right btn-floating btn-large waves-effect waves-light red">
                                <i class="fas fa-arrow-up"></i>
                            </button>
                        </form>
                    </div>
                    <br>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div id="message" style="color:azure"></div>
                            <form action="{{Route('ResAjax')}}" class="upload" enctype='multipart/form-data' method="POST">
                                @csrf
                                <div class="uploadOuter">
                                    <span class="dragBox" >
                                    <span id="TextList">glissez-deposez votre fichier .Math ou .Jar ici</span>
                                    <br>
                                    <strong>OU</strong>
                                    <br>
                                    <label for="uploadFile" class="btn btn-primary">Chercher votre fichier</label>
                                    <input type="file" onChange="dragNdrop(event)" accept='.mat, .jar' name="files" ondragover="drag(event)" ondrop="drop()" id="uploadFile"/>
                                    </span>
                                    </div>
                                <input class="btn btn-primary" id='submitbtn' type="submit">
                            </form>
                            <br><br>
                            <h3 style="color:white">Liste des fichiers :</h3>
                            <div style="color:white; border: 1px solid #ff0000 " id='listDesTel'></div>
                        </div>
                    </div> 
                    @else
                    <div class="alert alert-light center" role="alert" style="font-size : 22px">
                        Il faut que tu sois connectée pour avoir acces au formulaire => <button class="waves-effect waves-light btn blue-grey darken-4" ><a href={{route('home')}}>Connexion</a></button>
                    </div>
                    @endif
            </div>
            <div class="card-action center textWhite">
                    © Copyright 2019 By Di-ANALYSE SIGNAL
            </div>
        </div>
    </div>
    <div class="col-2 hide-on-med-and-down"></div>
</div>
</div>
@endsection
@section('jsContent')
<script>

var regexpMath = new RegExp('[.]mat$');
var regexpJar = new RegExp('[.]jar$');

var content;
var title = []

function dragNdrop(event) {
  for (var i = 0; i < event.target.files.length; i++) {
    var fileName = URL.createObjectURL(event.target.files[i]);
    if(regexpMath.test(event.target.files[i].name) || regexpJar.test(event.target.files[i].name)){
        console.log(event.target.files[i].name + ' true')
        console.log(event.target.files[i])
        content += event.target.files[i];
        title.push(event.target.files[i].name);
        title.push('<br>')
    }else{
      alert('Les Fichier doivent être en ".Jar" ou en ".Mat"')
      }
  }
    console.log(title[0].toString())
    update();
}

function update() {
  console.log(title)
  document.querySelector('#listDesTel').innerHTML = title + "<br>";
}

function drag(e) {
    document.getElementById('uploadFile').parentNode.className = 'draging dragBox';
}

function drop() {
    document.getElementById('uploadFile').parentNode.className = 'dragBox';
}






    let deferredPrompt;

        window.addEventListener('beforeinstallprompt', (e) => {
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault();
        // Stash the event so it can be triggered later.
        deferredPrompt = e;
        });

        window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can add to home screen
  btnAdd.style.display = 'block';
});

btnAdd.addEventListener('click', (e) => {
  // hide our user interface that shows our A2HS button
  btnAdd.style.display = 'none';
  // Show the prompt
  deferredPrompt.prompt();
  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
    .then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
      } else {
        console.log('User dismissed the A2HS prompt');
      }
      deferredPrompt = null;
    });
});

window.addEventListener('appinstalled', (evt) => {
  app.logEvent('a2hs', 'installed');
});


</script>
@endsection