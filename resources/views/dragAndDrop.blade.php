@extends('layouts.app')
@section('headerContent')
<style>

    </style>
@endsection
@section('content')
<div id="message" style="color:azure"></div>
<form action="{{Route('ResAjax')}}" class="upload" enctype='multipart/form-data' method="POST">
    @csrf
  <div class="uploadOuter">
    <span class="dragBox" >
      <span id="TextList">glissez-deposez votre fichier .Math ou .Jar ici</span>
      <br>
      <strong>OU</strong>
      <br>
      <label for="uploadFile" class="btn btn-primary">Chercher votre fichier</label>
      <input type="file" onChange="dragNdrop(event)" accept='.mat, .jar' name="files" ondragover="drag(event)" ondrop="drop()" id="uploadFile" multiple=""/>
    </span>
    </div>
  <input class="btn btn-primary" id='submitbtn' type="submit">
</form>
<br><br>

<h3 style="color:white">Liste des fichiers :</h3>
<hr><br>
<div style="color:white; border: 1px solid #ff0000 " id='listDesTel'></div>


@endsection
@section('jsContent')

<script>
var regexpMath = new RegExp('[.]mat$');
var regexpJar = new RegExp('[.]jar$');

var content;
var title = []

function dragNdrop(event) {
  for (var i = 0; i < event.target.files.length; i++) {
    var fileName = URL.createObjectURL(event.target.files[i]);
    if(regexpMath.test(event.target.files[i].name) || regexpJar.test(event.target.files[i].name)){
        console.log(event.target.files[i].name + ' true')
        console.log(event.target.files[i])
        content += event.target.files[i];
        title.push(event.target.files[i].name);
        title.push('<br>')
    }else{
      alert('Les Fichier doivent être en ".Jar" ou en ".Mat"')
      }
  }
    console.log(title[0].toString())
    update();
}

function update() {
  console.log(title)
  document.querySelector('#listDesTel').innerHTML = title + "<br>";
}

function drag(e) {
    document.getElementById('uploadFile').parentNode.className = 'draging dragBox';
}

function drop() {
    document.getElementById('uploadFile').parentNode.className = 'dragBox';
}
//
//$('form.upload').submit(function(e){
//  e.preventDefault();
//  url=$(this).attr('action');
//  type=$(this).attr('method');
//  var data = new FormData($(this)[0]);
//  data.append('id',$(this).attr('id'));
//  $.ajax({
//    url:url,
//    type:'post',
//    data:data,
//    processData: false,
//    contentType: false,
//    success:function(ans){
//      $('#message').text('working '+JSON.stringify(ans));
//    },
//    error:function(res){
//      $('#message').text('failed ' + JSON.stringify(res));
//    }
//  })
//})
//
</script>
@endsection
