@extends('layouts.app')
@section('content')
<div style="color: white;">
<table class="highlight">
   <thead>
      <tr>
         <th>Clef</th>
         <th>Valeur</th>
      </tr>
      
   </thead>
   <tbody>
      <tr>
            <td>URL</td>
            <td>{{$data->urlDb}}</td>
      </tr>
      <tr>
            <td>Nom Bdd</td>
            <td>{{$data->dbName}}</td>
      </tr>
      <tr>
            <td>Measurment</td>
            <td>{{$data->measurement}}</td>
      </tr>
      <tr>
            <td>EntityOperation</td>
            <td>{{$data->entityOperation}}</td>
      </tr>
      <tr>
            <td>Information</td>
            <td>{{$data->information}}</td>
      </tr>
      <tr>
            <td>Type d'entrainement</td>
            <td>{{$data->trainingType}}</td>
      </tr>
      <tr>
            <td>Date Debut</td>
            <td>{{$data->dateStart}}</td>
      </tr>
      <tr>
            <td>Heure Debut</td>
            <td>{{$data->hourStart}}</td>
      </tr>
      <tr>
         <td>Heure été/Hiver</td>
         <td>{{$data->tzStart}}</td>
      </tr>
      <tr>
         <td>Date Fin</td>
         <td>{{$data->dateEnd}}</td>
      </tr>
      <tr>
            <td>Heure Fin</td>
            <td>{{$data->hourEnd}}</td>
      </tr>
      <tr>
            <td>Heure été/Hiver</td>
            <td>{{$data->tzEnd}}</td>
      </tr>
      <tr>
            <td>label de Donnes</td>
            <td>{{$data->label}}</td>
      </tr>
      <tr>
            <td>ip Lps</td>
            <td>{{$data->serverIP}}</td>
      </tr>
      <tr>
            <td>Lps User</td>
            <td>{{$data->serverUsername}}</td>
      </tr>
      <tr>
            <td>Lps password</td>
            <td>{{$data->serverPassword}}</td>
      </tr>
   </tbody>
</table>

      <form action='http://127.0.0.1:1245' method="post">
            <input type="hidden" name="json" value='{{$jsonencode}}'>
        <button class="right btn-floating btn-large waves-effect waves-light red" type='submit'><i class="fas fa-arrow-up"></i></button>
           
      </form>
     </button> 

</div>
@endsection