@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Enregistrement') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nom') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phoneNumber" class="col-md-4 col-form-label text-md-right">Numéros de Telephone</label>

                            <div class="col-md-6">
                                <input id="phoneNumber" placeholder="ex : 060294853265" type="text" class="form-control{{ $errors->has('phoneNumber') ? ' is-invalid' : '' }}" name="phoneNumber" value="{{ old('phoneNumber') }}" required>

                                @if ($errors->has('phoneNumber'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phoneNumber') }}</strong>
                                        <strong>Attention a ne pas metre d'espace ou de tiret.</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="entreprise" class="col-md-4 col-form-label text-md-right">Nom de l'entreprise</label>

                            <div class="col-md-6">
                                <input id="entreprise" type="text" class="form-control{{ $errors->has('entreprise') ? ' is-invalid' : '' }}" name="entreprise" value="{{ old('entreprise') }}" required>

                                @if ($errors->has('entreprise'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('entreprise') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmer votre mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="urlInfluxDB" class="col-md-4 col-form-label text-md-right">{{ __('urlInfluxDB') }}</label>

                            <div class="col-md-6">
                                <input id="urlInfluxDB" type="text" class="form-control" name="urlInfluxDB" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="IpServer" class="col-md-4 col-form-label text-md-right">{{ __('IpServer') }}</label>

                            <div class="col-md-6">
                                <input id="IpServer" type="text" class="form-control" name="IpServer" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="LPSUser" class="col-md-4 col-form-label text-md-right">{{ __('LPSUser') }}</label>

                            <div class="col-md-6">
                                <input id="LPSUser" type="text" class="form-control" name="LPSUser" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="LPSPassword" class="col-md-4 col-form-label text-md-right">{{ __('LPSPassword') }}</label>

                            <div class="col-md-6">
                                <input id="LPSPassword" type="text" class="form-control" name="LPSPassword" required>
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="waves-effect waves-light btn red darken-4">
                                    {{ __('Enregistrement') }}
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
