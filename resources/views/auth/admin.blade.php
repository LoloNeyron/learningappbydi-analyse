@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col 12">
        <table class="striped" style="background-color: grey">
            <thead>
              <tr>
                <th>Name</th>
                <th>Entreprise</th>
                <th>Email</th>
                <th>Numéro de telephone</th>
            </tr>
          </thead>
      
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->entreprise}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->number}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</div>


@endsection