<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="/font/Nunito-Regular.ttf" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/materialize.min.css">

    <!-- Styles -->

    <link rel="stylesheet" href="/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="/font/fa-solid-900.ttf">
    <link rel="stylesheet" href="/font/fa-brands-400.ttf">
    <link rel="stylesheet" href="/font/fa-regular-400.ttf">
    <link rel="icon" type="image/png" href=".\assets\images\Lesly_logo.png" />
    <link rel="manifest" href="/manifest.webmanifest">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    @include('layouts.appCss')
    @yield('headerContent')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                    <!-- Authentication Links -->
                    @guest
                            <a class="nav-link mr-auto" href="{{ route('login') }}">{{ __('Connexion') }}</a>
                    @else
                      @foreach($admins as $admin)
                        @if($admin->isAdmin)

                          <a class="nav-link" href="{{ route('registerNewClient') }}">{{ __('Enregistrement') }}</a>
                          <a class="nav-link" href="{{Route('admin')}}">Admin</a>
                        @endif
                      @endforeach
                        <a id="navbar" class="nav-link ml-auto" href="#" role="button">
                        </a>
                        
                        <a class="ml-auto" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    
<footer class="mainfooter" role="contentinfo">
    <div class="footer-middle center">
    <div class="container center">
      <div class="row center noPadding">
        <div class="col-md-6 col-sm-12">
          <!--Column1-->
          <div class="footer-pad center">
            <h4>Adresse</h4>
            <hr>
            <address>
                <ul class="list-unstyled">
                    <li>Numériparc<br>
                        27 rue langenieux<br>
                        Roanne<br>
                        42300<br>
                    </li>
                    <li>
                        Phone: 04 26 24 90 19
                    </li>
                </ul>
                <br>
            </address>
          </div>
        </div>
        <hr>
        <div class="col-md-6 col-sm-12">
          <!--Column1-->
          <div class="footer-pad center">
            <h4>Sites Web</h4>
            <hr>
            <ul class="list-unstyled">
              <li><a href="http://dianalyse-signal.com/">DiAnalyse Signal Website</a></li>
            </ul>
          </div>
        </div>
        <br>
        <hr>
        <h4 class="text-xs-center center pFull" style="padding-left: calc(100vw - 75vw)"> &copy; Copyright 2019 - Di-ANALYSE SIGNAL</h4>
      </div>
    </div>
    </div>
  </footer>
    <script src="/js/jQuery3.3.1.js"></script>
    <script src="/js/materialize.min.js"></script>
   
    <script>

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
  });

  document.addEventListener('DOMContentLoaded', function () {
        var options = {
            format: 'yyyy-mm-dd'
        };
        var elems = document.querySelectorAll('.datepicker');
        var instance = M.Datepicker.init(elems, options);
    });

    </script>
     @yield('jsContent')
</body>
</html>
