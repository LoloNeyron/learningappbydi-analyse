<style>

  body{
    background-color: #2F2F2F;
    text-align: center;
  }

  .navbar {
    background-color: #202020;
    border-bottom: 1px solid red;
  }

  .navbar > a {
    color: white;
  }

  #app > nav > div > a, #navbarSupportedContent > ul.navbar-nav.ml-auto > li:nth-child(1) > a, #navbarSupportedContent > ul.navbar-nav.ml-auto > li:nth-child(2) > a {
    color: white;
  }

  #app {
    min-height: 99vh;
  }

  .noPadding{
    padding: 0;
    margin: 0;
  }

  footer{
  color: white;
}
footer a{
  color: #bfffff;
}
footer a:hover{
  color: white;
}

.footer-bottom{
  background: #202020;
  padding-top: 1em;
}
.footer-top{
  background: #202020;  
}
.footer-middle{
   background: #202020;
  padding-top: 2em;
  bottom: -20px;
  margin: 0;
  color: white;
}
/**Sub Navigation**/
.subnavigation-container{
  background: #3d6277;
}
.subnavigation .nav-link{
  color: white;
  font-weight: bold;
}
.subnavigation-container{
  text-align: center;
}
.subnavigation-container .navbar{
  display: inline-block;
  margin-bottom: -6px; /* Inline-block margin offffset HACK -Gilron */
}
.col-subnav a{
  padding: 1rem 1rem;
  color: white;
  font-weight: bold;
}
.col-subnav .active{
  border-top:5px solid orange;
 background: white;
  color: black;
}

.pFull {
  display:block;
}

.imgNav {
  width: 70px;
}

</style>