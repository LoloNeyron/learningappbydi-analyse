@extends('layouts.app')

@section('headerContent')
    @include('cssPage.homepagecss')
@endsection

@section('content')
<?php 
if(isset($_GET['mwdata']) && isset($_GET['mwsize']) && isset($_GET['mwtype'])){
  echo "<div class='alert alert-success center' role='alert' style='font-size : 22px'>Votre Json a bien été envoyé, merci de nous faire confiance.</div>";
}else{
  echo "<div class='alert alert-danger center' role='alert' style='font-size : 22px'>Une erreure est survenue pendant votre envoie. Veuillez verifier les informations.</div>";
};
?>

<button type="submit" class="waves-effect waves-light btn blue-grey darken-4"><a href={{Route('homepage')}}>Retour a l'accueil</a></button>
@endsection