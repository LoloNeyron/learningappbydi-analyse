<style>
  hr {
    border-color: red !important;
  }

  .input-field input:focus + label {
   color: red !important;
 }
 /* label underline focus color */
 .row .input-field input:focus {
   border-bottom: 1px solid red !important;
   box-shadow: 0 1px 0 0 red !important
 }

.grey8{
  background-color: #2F2F2F;
}
.grey7 {
  background-color: #3F3F3F;
}

h1{
color: white;
}
 label {
  color:White;
  font-size: 14px;
  margin: 0;
 }

 .textWhite{
   color:white;
 }

.greyCyclo {
  background-color: #202020 !important;
}

.datepicker-calendar-container{
    background-color: #232323 !important;
}

.modal {
  background-color: #ffffff00 !important;
  box-shadow: none;
}

#app > nav > div > button{
  color:red;
  border-color: white;
}

#app > main > div > div > div.col-lg-8.col-md-12 > div > div.card-content.white-text > form > div:nth-child(3) > div.input-flied.col-8 > div > input,
#app > main > div > div > div.col-lg-8.col-md-12 > div > div.card-content.white-text > form > div:nth-child(7) > div.input-flied.col-8 > div > input,
#app > main > div > div > div.col-lg-8.col-md-12 > div > div.card-content.white-text > form > div:nth-child(13) > div.input-flied.col-8 > div > input,
#app > main > div > div > div.col-lg-8.col-md-12 > div > div.card-content.white-text > form > div:nth-child(17) > div.input-flied.col-8 > div > input,
#hourStart,
#hourEnd{
  color: white !important;
}

#bdd > div > div:nth-child(2) > div.input-flied.col-8 > div > input,
#bdd > div > div:nth-child(6) > div.input-flied.col-8 > div > input,
#CollExtract > div > div:nth-child(4) > div.input-flied.col-8 > div > input,
#CollExtract > div > div:nth-child(8) > div.input-flied.col-8 > div > input,
 {
   background-color: #ffffff10;
  color: white !important;
}

 form > .row {
  border-radius: 4px;
  border-left: 3px solid red;
  padding-left : 5px;
  background-color: #ffffff15;
 }

 /* label color */
 .input-field label {
     color: #d50000 !important;
   }
   /* label focus color */
   .input-field input[type=text]:focus + label {
     color: #d50000 !important;
   }
   /* label underline focus color */
   .input-field input[type=text]:focus {
     border-bottom: 1px solid #d50000 !important;
     box-shadow: 0 1px 0 0 #d50000 !important;
   }
   /* valid color */
   .input-field input[type=text].valid {
     border-bottom: 1px solid #d50000 !important;
     box-shadow: 0 1px 0 0 #d50000 !important;
   }
   /* invalid color */
   .input-field input[type=text].invalid {
     border-bottom: 1px solid #d50000 !important;
     box-shadow: 0 1px 0 0 #d50000 !important;
   }
   /* icon prefix focus color */
   .input-field .prefix.active {
     color: #d50000 !important;
   }

   h6 {
     text-decoration: none;
   }
</style>