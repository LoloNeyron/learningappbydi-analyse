<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomepageController@index')->name('homepage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::post('/jsonmaker', 'JsonController@JsonMaker')->name('JsonMaker');

Route::get('/repLesly', 'ResponseLeslyController@index')->name('LeslyResponse');

Route::get('/RegisterNewClient', 'RegisterNewClientController@index')->name('registerNewClient');

Route::get('/DragAndDrop', 'draganddropController@index')->name('DragAndDrop');

Route::post('/responseAjax', 'UploadController@Store')->name('ResAjax');







